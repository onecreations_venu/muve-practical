<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DriverVehiclesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DriverVehiclesTable Test Case
 */
class DriverVehiclesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DriverVehiclesTable
     */
    public $DriverVehicles;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.driver_vehicles',
        'app.vehicle_type',
        'app.drivers'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('DriverVehicles') ? [] : ['className' => DriverVehiclesTable::class];
        $this->DriverVehicles = TableRegistry::getTableLocator()->get('DriverVehicles', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DriverVehicles);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
