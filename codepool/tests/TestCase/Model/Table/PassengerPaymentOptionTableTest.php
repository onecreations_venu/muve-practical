<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PassengerPaymentOptionTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PassengerPaymentOptionTable Test Case
 */
class PassengerPaymentOptionTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PassengerPaymentOptionTable
     */
    public $PassengerPaymentOption;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.passenger_payment_option'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PassengerPaymentOption') ? [] : ['className' => PassengerPaymentOptionTable::class];
        $this->PassengerPaymentOption = TableRegistry::getTableLocator()->get('PassengerPaymentOption', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PassengerPaymentOption);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
