<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\VehicleTypeTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\VehicleTypeTable Test Case
 */
class VehicleTypeTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\VehicleTypeTable
     */
    public $VehicleType;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.vehicle_type'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('VehicleType') ? [] : ['className' => VehicleTypeTable::class];
        $this->VehicleType = TableRegistry::getTableLocator()->get('VehicleType', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->VehicleType);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
