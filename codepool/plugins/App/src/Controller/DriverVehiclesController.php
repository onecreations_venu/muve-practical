<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DriverVehicles Controller
 *
 * @property \App\Model\Table\DriverVehiclesTable $DriverVehicles
 *
 * @method \App\Model\Entity\DriverVehicle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DriverVehiclesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['VehicleType', 'Drivers']
        ];
        $driverVehicles = $this->paginate($this->DriverVehicles);

        $this->set(compact('driverVehicles'));
    }

    /**
     * View method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $driverVehicle = $this->DriverVehicles->get($id, [
            'contain' => ['VehicleType', 'Drivers']
        ]);

        $this->set('driverVehicle', $driverVehicle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $driverVehicle = $this->DriverVehicles->newEntity();
        if ($this->request->is('post')) {
            $driverVehicle = $this->DriverVehicles->patchEntity($driverVehicle, $this->request->getData());
            if ($this->DriverVehicles->save($driverVehicle)) {
                $this->Flash->success(__('The driver vehicle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver vehicle could not be saved. Please, try again.'));
        }
        $vehicleType = $this->DriverVehicles->VehicleType->find('list', ['limit' => 200]);
        $drivers = $this->DriverVehicles->Drivers->find('list', ['limit' => 200]);
        $this->set(compact('driverVehicle', 'vehicleType', 'drivers'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $driverVehicle = $this->DriverVehicles->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $driverVehicle = $this->DriverVehicles->patchEntity($driverVehicle, $this->request->getData());
            if ($this->DriverVehicles->save($driverVehicle)) {
                $this->Flash->success(__('The driver vehicle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The driver vehicle could not be saved. Please, try again.'));
        }
        $vehicleType = $this->DriverVehicles->VehicleType->find('list', ['limit' => 200]);
        $drivers = $this->DriverVehicles->Drivers->find('list', ['limit' => 200]);
        $this->set(compact('driverVehicle', 'vehicleType', 'drivers'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Driver Vehicle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $driverVehicle = $this->DriverVehicles->get($id);
        if ($this->DriverVehicles->delete($driverVehicle)) {
            $this->Flash->success(__('The driver vehicle has been deleted.'));
        } else {
            $this->Flash->error(__('The driver vehicle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
