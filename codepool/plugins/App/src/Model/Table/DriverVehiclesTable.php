<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DriverVehicles Model
 *
 * @property \App\Model\Table\VehicleTypeTable|\Cake\ORM\Association\BelongsTo $VehicleType
 * @property \App\Model\Table\DriversTable|\Cake\ORM\Association\BelongsTo $Drivers
 *
 * @method \App\Model\Entity\DriverVehicle get($primaryKey, $options = [])
 * @method \App\Model\Entity\DriverVehicle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DriverVehicle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DriverVehicle|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DriverVehicle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DriverVehicle findOrCreate($search, callable $callback = null, $options = [])
 */
class DriverVehiclesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('driver_vehicles');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('VehicleType', [
            'foreignKey' => 'type_id',
            'className' => 'App.VehicleType'
        ]);
        $this->belongsTo('Drivers', [
            'foreignKey' => 'driver_id',
            'className' => 'App.Drivers'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('model')
            ->maxLength('model', 45)
            ->allowEmpty('model');

        $validator
            ->scalar('number_plate')
            ->maxLength('number_plate', 45)
            ->allowEmpty('number_plate');

        $validator
            ->scalar('current_location')
            ->maxLength('current_location', 45)
            ->allowEmpty('current_location');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['type_id'], 'VehicleType'));
        $rules->add($rules->existsIn(['driver_id'], 'Drivers'));

        return $rules;
    }
}
