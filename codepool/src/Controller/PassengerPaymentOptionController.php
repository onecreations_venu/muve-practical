<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PassengerPaymentOption Controller
 *
 * @property \App\Model\Table\PassengerPaymentOptionTable $PassengerPaymentOption
 *
 * @method \App\Model\Entity\PassengerPaymentOption[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PassengerPaymentOptionController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $passengerPaymentOption = $this->paginate($this->PassengerPaymentOption);

        $this->set(compact('passengerPaymentOption'));
    }

    /**
     * View method
     *
     * @param string|null $id Passenger Payment Option id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $passengerPaymentOption = $this->PassengerPaymentOption->get($id, [
            'contain' => []
        ]);

        $this->set('passengerPaymentOption', $passengerPaymentOption);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $passengerPaymentOption = $this->PassengerPaymentOption->newEntity();
        if ($this->request->is('post')) {
            $passengerPaymentOption = $this->PassengerPaymentOption->patchEntity($passengerPaymentOption, $this->request->getData());
            if ($this->PassengerPaymentOption->save($passengerPaymentOption)) {
                $this->Flash->success(__('The passenger payment option has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The passenger payment option could not be saved. Please, try again.'));
        }
        $this->set(compact('passengerPaymentOption'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Passenger Payment Option id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $passengerPaymentOption = $this->PassengerPaymentOption->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $passengerPaymentOption = $this->PassengerPaymentOption->patchEntity($passengerPaymentOption, $this->request->getData());
            if ($this->PassengerPaymentOption->save($passengerPaymentOption)) {
                $this->Flash->success(__('The passenger payment option has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The passenger payment option could not be saved. Please, try again.'));
        }
        $this->set(compact('passengerPaymentOption'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Passenger Payment Option id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $passengerPaymentOption = $this->PassengerPaymentOption->get($id);
        if ($this->PassengerPaymentOption->delete($passengerPaymentOption)) {
            $this->Flash->success(__('The passenger payment option has been deleted.'));
        } else {
            $this->Flash->error(__('The passenger payment option could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
