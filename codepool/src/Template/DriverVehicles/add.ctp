<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DriverVehicle $driverVehicle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Driver Vehicles'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Drivers'), ['controller' => 'Drivers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Driver'), ['controller' => 'Drivers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="driverVehicles form large-9 medium-8 columns content">
    <?= $this->Form->create($driverVehicle) ?>
    <fieldset>
        <legend><?= __('Add Driver Vehicle') ?></legend>
        <?php
            echo $this->Form->control('type_id', ['options' => $vehicleType, 'empty' => true]);
            echo $this->Form->control('model');
            echo $this->Form->control('number_plate');
            echo $this->Form->control('current_location');
            echo $this->Form->control('driver_id', ['options' => $drivers, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
