<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DriverVehicle[]|\Cake\Collection\CollectionInterface $driverVehicles
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Driver Vehicle'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Drivers'), ['controller' => 'Drivers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Driver'), ['controller' => 'Drivers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="driverVehicles index large-9 medium-8 columns content">
    <h3><?= __('Driver Vehicles') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('type_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('model') ?></th>
                <th scope="col"><?= $this->Paginator->sort('number_plate') ?></th>
                <th scope="col"><?= $this->Paginator->sort('current_location') ?></th>
                <th scope="col"><?= $this->Paginator->sort('driver_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($driverVehicles as $driverVehicle): ?>
            <tr>
                <td><?= $this->Number->format($driverVehicle->id) ?></td>
                <td><?= $driverVehicle->has('vehicle_type') ? $this->Html->link($driverVehicle->vehicle_type->id, ['controller' => 'VehicleType', 'action' => 'view', $driverVehicle->vehicle_type->id]) : '' ?></td>
                <td><?= h($driverVehicle->model) ?></td>
                <td><?= h($driverVehicle->number_plate) ?></td>
                <td><?= h($driverVehicle->current_location) ?></td>
                <td><?= $driverVehicle->has('driver') ? $this->Html->link($driverVehicle->driver->id, ['controller' => 'Drivers', 'action' => 'view', $driverVehicle->driver->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $driverVehicle->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $driverVehicle->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $driverVehicle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverVehicle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
