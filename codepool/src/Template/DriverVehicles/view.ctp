<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\DriverVehicle $driverVehicle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Driver Vehicle'), ['action' => 'edit', $driverVehicle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Driver Vehicle'), ['action' => 'delete', $driverVehicle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverVehicle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Driver Vehicles'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver Vehicle'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['controller' => 'VehicleType', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Drivers'), ['controller' => 'Drivers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver'), ['controller' => 'Drivers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="driverVehicles view large-9 medium-8 columns content">
    <h3><?= h($driverVehicle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Vehicle Type') ?></th>
            <td><?= $driverVehicle->has('vehicle_type') ? $this->Html->link($driverVehicle->vehicle_type->id, ['controller' => 'VehicleType', 'action' => 'view', $driverVehicle->vehicle_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Model') ?></th>
            <td><?= h($driverVehicle->model) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Number Plate') ?></th>
            <td><?= h($driverVehicle->number_plate) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Current Location') ?></th>
            <td><?= h($driverVehicle->current_location) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Driver') ?></th>
            <td><?= $driverVehicle->has('driver') ? $this->Html->link($driverVehicle->driver->id, ['controller' => 'Drivers', 'action' => 'view', $driverVehicle->driver->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($driverVehicle->id) ?></td>
        </tr>
    </table>
</div>
