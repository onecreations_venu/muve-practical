<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Booking $booking
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Booking'), ['action' => 'edit', $booking->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Booking'), ['action' => 'delete', $booking->id], ['confirm' => __('Are you sure you want to delete # {0}?', $booking->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Bookings'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Drivers'), ['controller' => 'Drivers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver'), ['controller' => 'Drivers', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Passengers'), ['controller' => 'Passengers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Passenger'), ['controller' => 'Passengers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bookings view large-9 medium-8 columns content">
    <h3><?= h($booking->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Pickup Address') ?></th>
            <td><?= h($booking->pickup_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Drop Address') ?></th>
            <td><?= h($booking->drop_address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Driver') ?></th>
            <td><?= $booking->driver->first_name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Passenger') ?></th>
            <td><?= $booking->passenger->first_name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Total Distance') ?></th>
            <td><?= h($booking->total_distance) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bill Amount') ?></th>
            <td><?= h($booking->bill_amount) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nfp') ?></th>
            <td><?= h($booking->nfp) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Status') ?></th>
            <td><?= h($booking->status) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($booking->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created At') ?></th>
            <td><?= h($booking->created_at) ?></td>
        </tr>
    </table>
    <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <div id="map" style="width:100%; height: 500px"></div>


    <script type="text/javascript">

        var directionsDisplay;
        var directionsService = new google.maps.DirectionsService();

        var pickup_address = '<?php echo $booking->pickup_address ?>';
        var drop_address = '<?php echo  $booking->drop_address ?>';

            console.log(pickup_address);
        function initialize() {

            var mapOptions = {
                center: new google.maps.LatLng(6.927, 79.861),
                zoom: 12
            };

            var map = new google.maps.Map(document.getElementById("map"),
                mapOptions);

            directionsDisplay = new google.maps.DirectionsRenderer();
            directionsDisplay.setMap(map);

            calcRoute();
        }

        var ListBox2 = [
            { value: pickup_address},
            { value: drop_address }

        ];

        function calcRoute() {

            // var start = document.getElementById('start').value;
            var start = pickup_address;

            var waypts = [];

            //var end = document.getElementById('end').value;
            var end = drop_address;

            //var waypoints = []; // init an empty waypoints array
            //check list from listbox(?)

            for (var i = 1; i < ListBox2.length - 1; i++) {
                waypts.push({
                    location: ListBox2[i].value,
                    stopover: true
                });
            }

            var request = {
                origin: start,
                destination: end,
                waypoints: waypts,
                optimizeWaypoints: true,
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function (response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    var route = response.routes[0];
                } else {
                    console.log('status: ' + status);
                }
            });
        }

        google.maps.event.addDomListener(window, 'load', initialize);

    </script>

</div>
