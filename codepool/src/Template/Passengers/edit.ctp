<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Passenger $passenger
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $passenger->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $passenger->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Passengers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Passenger Payment Option'), ['controller' => 'PassengerPaymentOption', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Passenger Payment Option'), ['controller' => 'PassengerPaymentOption', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="passengers form large-9 medium-8 columns content">
    <?= $this->Form->create($passenger) ?>
    <fieldset>
        <legend><?= __('Edit Passenger') ?></legend>
        <?php
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone');
            echo $this->Form->control('payment_option_id', ['options' => $passengerPaymentOption, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
