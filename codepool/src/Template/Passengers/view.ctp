<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Passenger $passenger
 */
echo $this->Paginator->limitControl([25 => 25, 50 => 50], $passenger->perPage);
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Passenger'), ['action' => 'edit', $passenger->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Passenger'), ['action' => 'delete', $passenger->id], ['confirm' => __('Are you sure you want to delete # {0}?', $passenger->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Passengers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Passenger'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Passenger Payment Option'), ['controller' => 'PassengerPaymentOption', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Passenger Payment Option'), ['controller' => 'PassengerPaymentOption', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="passengers view large-9 medium-8 columns content">
    <h3><?= h($passenger->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($passenger->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($passenger->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($passenger->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Passenger Payment Option') ?></th>
            <td><?= $passenger->has('passenger_payment_option') ? $this->Html->link($passenger->passenger_payment_option->id, ['controller' => 'PassengerPaymentOption', 'action' => 'view', $passenger->passenger_payment_option->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($passenger->id) ?></td>
        </tr>
    </table>
</div>
