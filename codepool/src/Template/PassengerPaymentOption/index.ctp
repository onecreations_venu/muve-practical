<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PassengerPaymentOption[]|\Cake\Collection\CollectionInterface $passengerPaymentOption
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Passenger Payment Option'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="passengerPaymentOption index large-9 medium-8 columns content">
    <h3><?= __('Passenger Payment Option') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('payment_option') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($passengerPaymentOption as $passengerPaymentOption): ?>
            <tr>
                <td><?= $this->Number->format($passengerPaymentOption->id) ?></td>
                <td><?= h($passengerPaymentOption->payment_option) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $passengerPaymentOption->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $passengerPaymentOption->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $passengerPaymentOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $passengerPaymentOption->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
