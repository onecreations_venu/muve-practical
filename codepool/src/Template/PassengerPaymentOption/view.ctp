<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PassengerPaymentOption $passengerPaymentOption
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Passenger Payment Option'), ['action' => 'edit', $passengerPaymentOption->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Passenger Payment Option'), ['action' => 'delete', $passengerPaymentOption->id], ['confirm' => __('Are you sure you want to delete # {0}?', $passengerPaymentOption->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Passenger Payment Option'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Passenger Payment Option'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="passengerPaymentOption view large-9 medium-8 columns content">
    <h3><?= h($passengerPaymentOption->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Payment Option') ?></th>
            <td><?= h($passengerPaymentOption->payment_option) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($passengerPaymentOption->id) ?></td>
        </tr>
    </table>
</div>
