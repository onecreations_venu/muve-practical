<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\VehicleType $vehicleType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Vehicle Type'), ['action' => 'edit', $vehicleType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Vehicle Type'), ['action' => 'delete', $vehicleType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $vehicleType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Vehicle Type'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Vehicle Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="vehicleType view large-9 medium-8 columns content">
    <h3><?= h($vehicleType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Vehicle Type') ?></th>
            <td><?= h($vehicleType->vehicle_type) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($vehicleType->id) ?></td>
        </tr>
    </table>
</div>
