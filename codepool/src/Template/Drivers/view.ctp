<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Driver $driver
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Driver'), ['action' => 'edit', $driver->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Driver'), ['action' => 'delete', $driver->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driver->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Drivers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Bookings'), ['controller' => 'Bookings', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Booking'), ['controller' => 'Bookings', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Driver Vehicles'), ['controller' => 'DriverVehicles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Driver Vehicle'), ['controller' => 'DriverVehicles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="drivers view large-9 medium-8 columns content">
    <h3><?= h($driver->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($driver->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($driver->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($driver->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($driver->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Bookings') ?></h4>
        <?php if (!empty($driver->bookings)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Pickup Address') ?></th>
                <th scope="col"><?= __('Drop Address') ?></th>
                <th scope="col"><?= __('Driver Id') ?></th>
                <th scope="col"><?= __('Customer Id') ?></th>
                <th scope="col"><?= __('Total Distance') ?></th>
                <th scope="col"><?= __('Bill Amount') ?></th>
                <th scope="col"><?= __('Nfp') ?></th>
                <th scope="col"><?= __('Status') ?></th>
                <th scope="col"><?= __('Created At') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($driver->bookings as $bookings): ?>
            <tr>
                <td><?= h($bookings->id) ?></td>
                <td><?= h($bookings->pickup_address) ?></td>
                <td><?= h($bookings->drop_address) ?></td>
                <td><?= h($bookings->driver_id) ?></td>
                <td><?= h($bookings->customer_id) ?></td>
                <td><?= h($bookings->total_distance) ?></td>
                <td><?= h($bookings->bill_amount) ?></td>
                <td><?= h($bookings->nfp) ?></td>
                <td><?= h($bookings->status) ?></td>
                <td><?= h($bookings->created_at) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Bookings', 'action' => 'view', $bookings->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Bookings', 'action' => 'edit', $bookings->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Bookings', 'action' => 'delete', $bookings->id], ['confirm' => __('Are you sure you want to delete # {0}?', $bookings->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Driver Vehicles') ?></h4>
        <?php if (!empty($driver->driver_vehicles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Type Id') ?></th>
                <th scope="col"><?= __('Model') ?></th>
                <th scope="col"><?= __('Number Plate') ?></th>
                <th scope="col"><?= __('Current Location') ?></th>
                <th scope="col"><?= __('Driver Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($driver->driver_vehicles as $driverVehicles): ?>
            <tr>
                <td><?= h($driverVehicles->id) ?></td>
                <td><?= h($driverVehicles->type_id) ?></td>
                <td><?= h($driverVehicles->model) ?></td>
                <td><?= h($driverVehicles->number_plate) ?></td>
                <td><?= h($driverVehicles->current_location) ?></td>
                <td><?= h($driverVehicles->driver_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'DriverVehicles', 'action' => 'view', $driverVehicles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'DriverVehicles', 'action' => 'edit', $driverVehicles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'DriverVehicles', 'action' => 'delete', $driverVehicles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $driverVehicles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
