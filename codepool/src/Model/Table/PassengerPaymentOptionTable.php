<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PassengerPaymentOption Model
 *
 * @method \App\Model\Entity\PassengerPaymentOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PassengerPaymentOption findOrCreate($search, callable $callback = null, $options = [])
 */
class PassengerPaymentOptionTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('passenger_payment_option');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('payment_option')
            ->maxLength('payment_option', 45)
            ->allowEmpty('payment_option');

        return $validator;
    }
}
