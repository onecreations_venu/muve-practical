<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * VehicleType Model
 *
 * @method \App\Model\Entity\VehicleType get($primaryKey, $options = [])
 * @method \App\Model\Entity\VehicleType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\VehicleType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\VehicleType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VehicleType|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\VehicleType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\VehicleType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\VehicleType findOrCreate($search, callable $callback = null, $options = [])
 */
class VehicleTypeTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('vehicle_type');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('vehicle_type')
            ->maxLength('vehicle_type', 45)
            ->allowEmpty('vehicle_type');

        return $validator;
    }
}
