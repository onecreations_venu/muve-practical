<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DriverVehicle Entity
 *
 * @property int $id
 * @property int $type_id
 * @property string $model
 * @property string $number_plate
 * @property string $current_location
 * @property string $driver_id
 *
 * @property \App\Model\Entity\VehicleType $vehicle_type
 * @property \App\Model\Entity\Driver $driver
 */
class DriverVehicle extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'type_id' => true,
        'model' => true,
        'number_plate' => true,
        'current_location' => true,
        'driver_id' => true,
        'vehicle_type' => true,
        'driver' => true
    ];
}
