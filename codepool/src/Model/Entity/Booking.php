<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Booking Entity
 *
 * @property int $id
 * @property string $pickup_address
 * @property string $drop_address
 * @property int $driver_id
 * @property int $customer_id
 * @property string $total_distance
 * @property string $bill_amount
 * @property string $nfp
 * @property string $status
 * @property \Cake\I18n\FrozenTime $created_at
 *
 * @property \App\Model\Entity\Driver $driver
 * @property \App\Model\Entity\Passenger $passenger
 */
class Booking extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'pickup_address' => true,
        'drop_address' => true,
        'driver_id' => true,
        'customer_id' => true,
        'total_distance' => true,
        'bill_amount' => true,
        'nfp' => true,
        'status' => true,
        'created_at' => true,
        'driver' => true,
        'passenger' => true
    ];
}
